import {Link} from 'react-router'
import classNames from 'classnames'
import React from 'react'
import OfferDialog from './offer-dialog'
import OfferMessages from './offer-messages'



export default (props)=> (
  <div className={classNames({
      "offer-list__item card": true,
      "hidden": props.offer.status.isHidden})}>
    <div className="card-content">
      <Link className="item-title"
            to={`/offers/${props.offer.data.id}`}>
        {props.offer.data.executor.first_name} предлагает свои услуги
        (бюджет {props.offer.data.messages[0].price} рублей)
      </Link>
    </div>
  </div>
)
