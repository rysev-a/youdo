import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import view from './view'
import actions from './actions'


const filterOffers = (offers, currentUser) => 
  offers.filter(offer => (
    offer.data.executor.id == currentUser.data.id ||
    offer.data.task.customer_id == currentUser.data.id
  ))


const mapStateToProps = (state) => {
  return {
    offers: {
      ...state.offers.list,
      data: filterOffers(state.offers.list.data, state.profile.current)
    },
    task: state.tasks.item,
    currentUser: state.profile.current
  };
};

const mapDispatchToProps = (dispatch)=> (
  bindActionCreators(actions, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(view)


