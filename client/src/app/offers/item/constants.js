export default {
  FETCH_OFFER_ITEM:         'fetch offer item',
  FETCH_OFFER_ITEM_SUCCESS: 'fetch offer item success',
  FETCH_OFFER_ITEM_ERROR:   'fetch offer item error',

  RESET_OFFER_ITEM:         'reset offer item',
  UPDATE_OFFER_DIALOG:      'update offer dialog',

  ACCEPT_OFFER_DIALOG: 'accept offer dialog',
  ACCEPT_OFFER_DIALOG_SUCCESS: 'accept offer dialog success',
  ACCEPT_OFFER_DIALOG_ERROR: 'accept offer dialog error',

  CONFIRM_OFFER_DIALOG: 'confirm offer dialog',
  CONFIRM_OFFER_DIALOG_SUCCESS: 'confirm offer dialog success',
  CONFIRM_OFFER_DIALOG_ERROR: 'confirm offer dialog error',

  COMPLETE_OFFER_DIALOG: 'complete offer dialog',
  COMPLETE_OFFER_DIALOG_SUCCESS: 'complete offer dialog success',
  COMPLETE_OFFER_DIALOG_ERROR: 'complete offer dialog error'
}
