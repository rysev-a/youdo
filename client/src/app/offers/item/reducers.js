import {combineReducers} from 'redux'
import constants from 'app/constants'


const defaultState = ()=> {
  return {};
};


function data(state=defaultState(), action) {
  switch (action.type) {
    case constants.FETCH_OFFER_ITEM_SUCCESS:
      return action.payload;

    case constants.ACCEPT_OFFER_DIALOG_SUCCESS:
    case constants.CONFIRM_OFFER_DIALOG_SUCCESS:
    case constants.COMPLETE_OFFER_DIALOG_SUCCESS:
      return action.payload;

    default:
      return state;
  }
}

const defaultDialogState = ()=> ({
  content: '',
});

function dialog(state=defaultDialogState(), action) {
  switch (action.type) {
    case constants.UPDATE_OFFER_DIALOG:
      let {field, value} = action.payload;
      return Object.assign({}, state, {[field]: value});

    case constants.FETCH_OFFER_ITEM_SUCCESS:
      let price = action.payload.messages.slice(-1)[0].price;
      return Object.assign({}, state, {price})

    default:
      return state;
  }
}

function status(state={loaded: false}, action) {
  switch (action.type) {
    case constants.FETCH_OFFER_ITEM_SUCCESS:
      return {loaded: true};

    case constants.FETCH_OFFER_ITEM:
    case constants.RESET_OFFER_ITEM:
      return {loaded: false};


    default:
      return state;
  }
}

export default combineReducers({data, status, dialog})
