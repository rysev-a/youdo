import constants from 'app/constants';
import api from '../api';
import requestAction from 'app/core/helpers/request-action';


export default {
  fetch: (id)=> {
    return requestAction(api.item(id), {
      request: constants.FETCH_OFFER_ITEM,
      success: constants.FETCH_OFFER_ITEM_SUCCESS,
      error:   constants.FETCH_OFFER_ITEM_ERROR
    })
  },

  reset: ()=> ({type: constants.RESET_OFFER_ITEM}),

  updateDialog: (field, value) => ({
    type: constants.UPDATE_OFFER_DIALOG,
    payload: {field, value}
  }),

  accept: (offer, customer)=> {
    return (dispatch)=> {
      api.accept(offer.data.id, {
        message: {
          offer_id: offer.data.id,
          sender_id: customer.data.id,
          content: offer.dialog.content,
          price: offer.dialog.price
        }
      })
        .then((response) => response.json())
        .then((json) => {
          dispatch({
            type: constants.ACCEPT_OFFER_DIALOG_SUCCESS,
            payload: json
          });
        })
    }
  },

  confirm: (offer, executor)=> {
    return (dispatch)=> {
      api.confirm(offer.data.id, {
        message: {
          offer_id: offer.data.id,
          sender_id: executor.data.id,
          content: offer.dialog.content,
          price: offer.dialog.price
        }
      })
        .then((response) => response.json())
        .then((json) => {
          dispatch({
            type: constants.CONFIRM_OFFER_DIALOG_SUCCESS,
            payload: json
          });
        })
    }
  },

  complete: (offer, executor)=> {
    return (dispatch)=> {
      api.complete(offer.data.id, {
        message: {
          offer_id: offer.data.id,
          sender_id: executor.data.id,
          content: offer.dialog.message
        }
      })
        .then((response) => response.json())
        .then((json) => {
          dispatch({
            type: constants.COMPLETE_OFFER_DIALOG_SUCCESS,
            payload: json
          });
          dispatch({
            type: constants.CLOSE_OFFER_DIALOG,
            payload: offer
          });
        })
    }
  }
}
