import classNames from 'classnames'
import React from 'react'
import {Link} from 'react-router'
import {Component} from 'react'
import Loader from 'app/core/components/loader'
import {getStatusText} from 'app/core/helpers/localization'
import Messages from './messages'
import Dialog from './dialog'


class OfferItemWrapper extends Component {
  componentDidMount () {
    const {id} = this.props.params;
    this.props.fetch(id);
  }

  componentWillUnmount () {
    this.props.reset();
  }

  render () {
    if (!this.props.offer.status.loaded) {
      return <Loader processing={true}/>
    }

    return (
      <div className="offer-item">
        <OfferItem {...this.props}/>
      </div>
    );
  }
}

class OfferItem extends Component {
  render () {
    let {offer} = this.props;
    let {task} = offer.data;

    return (
      <div className="container">
        <h2>Предложение к задаче
          <Link className="task-link" to={`/tasks/${offer.data.task.id}`} >
            {offer.data.task.name}
          </Link>
        </h2>
        <div className="card">
          <div className="card-content">
            <h3>Описание</h3>
            <div className="task-item__description">
              {task.description}
            </div>
            <h3>Начальный бюджет</h3>
            <div className="task-item__description">
              {task.price}
            </div>
            <h3>Обсуждение</h3>
            <div className="offer-item__messages">
              {Messages(offer.data.messages)}
            </div>
          </div>
        </div>
        <Dialog {...this.props} />
      </div>
    );
  }
}

export default OfferItemWrapper
