import React from 'react'


export default (messages)=> (
  <div className="message-list">
    {messages.map( message => (
      <div className="message" key={message.id}>
        <div className="message-sender">
          {message.sender.first_name} (бюджет {message.price}):
        </div>
        <div className="message-content">
          {message.content}
        </div>
      </div>
    ))}
  </div>
)
