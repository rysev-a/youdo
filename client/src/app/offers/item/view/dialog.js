import classNames from 'classnames'
import Loader from 'app/core/components/loader'
import React from 'react'


const isHidden = (props)=> {
  if (props.offer.data.status == 'waiting' && 
      props.currentUser.data.id == props.offer.data.executor.id) {
    return true;
  }


  if (props.offer.data.status == 'accepted' &&
      props.currentUser.data.id == props.offer.data.task.customer_id) {
    return true;
  }

  if (props.offer.data.status == 'running' &&
      props.currentUser.data.id == props.offer.data.task.executor_id) {
    return true;
  }

  if (props.offer.data.status == 'completed') {
    return true;
  }

  return false;
}

const buttons = (props)=> {
  if (props.offer.data.status == 'waiting') {
    return <a className="button buttom-primary"
       onClick={()=> props.accept(props.offer, props.currentUser)}>
       Отправить
    </a>
  }

  if (props.offer.data.status == 'accepted') {
    return <a className="button buttom-primary"
       onClick={()=> props.confirm(props.offer, props.currentUser)}>
       Согласиться
    </a>
  }

  if (props.offer.data.status == 'running') {
    return <a className="button buttom-primary"
       onClick={()=> props.complete(props.offer, props.currentUser)}>
       Сообщить о завершении
    </a>
  }
}


export default (props)=>
  <div className={classNames({
      "offer-dialog": true,
      "hidden": isHidden(props)
    })}>
    <div className="card">
      <div className="card-content">
        <div className="dialog-form">
          <div className={classNames({
                 "form-item": true,
                 "hidden": ["waiting"].indexOf(props.offer.data.status) == -1
               })}>
            <input type="number" className="form-control"
                   value={props.offer.dialog.price}
                   onChange={(e) =>
                    props.updateDialog('price', e.target.value)}/>
            <label className="form-label">Ваша цена</label>
          </div>
          <div className={classNames({
                 "form-item": true,
                 "hidden": props.offer.data.status == 'running'
               })}>
            <textarea className="form-control"
                      defaultValue=""
                      onChange={(e) => 
                        props.updateDialog('content', e.target.value)}/>
            <label className="form-label">Введите сообщение</label>
          </div>
        </div>

        <div className="dialog-buttons">
          {buttons(props)}
        </div>
      </div>
    </div>
  </div>

