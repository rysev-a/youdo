import React from 'react'
import {Component} from 'react'
import {render} from 'react-dom'
import {Route} from 'react-router'

import OfferItem from 'app/offers/item'


export default <Route path='/offers'>
  <Route path="offers">
    <Route path="/offers/:id" component={OfferItem}/>
  </Route>
</Route>
