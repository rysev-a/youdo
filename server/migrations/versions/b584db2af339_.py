"""empty message

Revision ID: b584db2af339
Revises: 9f1423459845
Create Date: 2017-05-21 13:28:51.215955

"""

# revision identifiers, used by Alembic.
revision = 'b584db2af339'
down_revision = '9f1423459845'

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.add_column('messages', sa.Column('price', sa.Integer(), nullable=True))
    op.drop_column('offers', 'price')
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.add_column('offers', sa.Column('price', sa.INTEGER(), autoincrement=False, nullable=True))
    op.drop_column('messages', 'price')
    ### end Alembic commands ###
