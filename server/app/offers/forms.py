from wtforms_alchemy import ModelForm
from wtforms import IntegerField, StringField
from wtforms.validators import DataRequired


class OfferForm(ModelForm):
    executor_id = IntegerField(validators=[DataRequired()])
    task_id = IntegerField(validators=[DataRequired()])

