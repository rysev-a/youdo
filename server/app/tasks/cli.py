import yaml
from flask_script import Manager
from .models import Task, TaskCategory
from ..notifications.models import Notification
from ..offers.models import Offer, Message
from ..database import db
from ..users.models import User


task_manager = Manager()
@task_manager.command
def clear():
    Notification.query.delete()
    Message.query.delete()
    Offer.query.delete()
    Task.query.delete()
    TaskCategory.query.delete()
    db.session.commit()

@task_manager.command
def mock():
    with open('app/tasks/mockup.yaml') as tasks:
        data = yaml.load(tasks)

    # create categories
    for category_data in data['task_categories']:
        new_category = TaskCategory(**category_data)
        db.session.add(new_category)
    db.session.commit()

    # get category indexes
    categories = dict((category.title, category.id)
        for category in TaskCategory.query.all())

    import time
    for task_data in data['tasks']:
        task_data['category_id'] = categories[task_data['category']]
        task_data['customer_id'] = User.query.first().id
        del task_data['category']
        new_task = Task(**task_data)
        db.session.add(new_task)
    db.session.commit()
