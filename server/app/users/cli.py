import yaml
from flask_script import Manager
from .models import User
from ..database import db


user_manager = Manager()
@user_manager.command
def clear():
    User.query.delete()

@user_manager.command
def mock():
    with open('app/users/mockup.yaml') as users:
        user_list_data = yaml.load(users)['users']

    for user_data in user_list_data:
        user = User(**user_data)
        db.session.add(user)
    db.session.commit()
